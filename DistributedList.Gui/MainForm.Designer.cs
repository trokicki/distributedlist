﻿namespace DistributedList
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.AddButton = new System.Windows.Forms.Button();
			this.AddIndexNud = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.AddValueNud = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.EditGroupbox = new System.Windows.Forms.GroupBox();
			this.EditButton = new System.Windows.Forms.Button();
			this.EditIndexNud = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.EditValueNud = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.SelectGroupbox = new System.Windows.Forms.GroupBox();
			this.SelectedValueLabel = new System.Windows.Forms.Label();
			this.SelectIndexNud = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.SelectButton = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.DeleteGroupbox = new System.Windows.Forms.GroupBox();
			this.DeleteButton = new System.Windows.Forms.Button();
			this.DeleteIndexNud = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.LengthLabel = new System.Windows.Forms.Label();
			this.CheckSizeButton = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.ConnectionSettingsGroupbox = new System.Windows.Forms.GroupBox();
			this.ListenPortTextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.ListenButton = new System.Windows.Forms.Button();
			this.HostPortTextbox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.HostAddressTextbox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.ConnectButton = new System.Windows.Forms.Button();
			this.LocalListContentLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AddIndexNud)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.AddValueNud)).BeginInit();
			this.EditGroupbox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.EditIndexNud)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.EditValueNud)).BeginInit();
			this.SelectGroupbox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.SelectIndexNud)).BeginInit();
			this.DeleteGroupbox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DeleteIndexNud)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.ConnectionSettingsGroupbox.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.AddButton);
			this.groupBox1.Controls.Add(this.AddIndexNud);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.AddValueNud);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 94);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(110, 106);
			this.groupBox1.TabIndex = 30;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Add";
			// 
			// AddButton
			// 
			this.AddButton.Location = new System.Drawing.Point(10, 74);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(89, 23);
			this.AddButton.TabIndex = 3;
			this.AddButton.Text = "Add";
			this.AddButton.UseVisualStyleBackColor = true;
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// AddIndexNud
			// 
			this.AddIndexNud.Location = new System.Drawing.Point(46, 48);
			this.AddIndexNud.Name = "AddIndexNud";
			this.AddIndexNud.Size = new System.Drawing.Size(53, 20);
			this.AddIndexNud.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(33, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Index";
			// 
			// AddValueNud
			// 
			this.AddValueNud.Location = new System.Drawing.Point(46, 23);
			this.AddValueNud.Name = "AddValueNud";
			this.AddValueNud.Size = new System.Drawing.Size(53, 20);
			this.AddValueNud.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(34, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Value";
			// 
			// EditGroupbox
			// 
			this.EditGroupbox.Controls.Add(this.EditButton);
			this.EditGroupbox.Controls.Add(this.EditIndexNud);
			this.EditGroupbox.Controls.Add(this.label3);
			this.EditGroupbox.Controls.Add(this.EditValueNud);
			this.EditGroupbox.Controls.Add(this.label4);
			this.EditGroupbox.Location = new System.Drawing.Point(124, 94);
			this.EditGroupbox.Name = "EditGroupbox";
			this.EditGroupbox.Size = new System.Drawing.Size(110, 106);
			this.EditGroupbox.TabIndex = 31;
			this.EditGroupbox.TabStop = false;
			this.EditGroupbox.Text = "Edit";
			// 
			// EditButton
			// 
			this.EditButton.Location = new System.Drawing.Point(10, 74);
			this.EditButton.Name = "EditButton";
			this.EditButton.Size = new System.Drawing.Size(89, 23);
			this.EditButton.TabIndex = 6;
			this.EditButton.Text = "Edit";
			this.EditButton.UseVisualStyleBackColor = true;
			this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
			// 
			// EditIndexNud
			// 
			this.EditIndexNud.Location = new System.Drawing.Point(46, 48);
			this.EditIndexNud.Name = "EditIndexNud";
			this.EditIndexNud.Size = new System.Drawing.Size(53, 20);
			this.EditIndexNud.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(7, 51);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(33, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Index";
			// 
			// EditValueNud
			// 
			this.EditValueNud.Location = new System.Drawing.Point(46, 23);
			this.EditValueNud.Name = "EditValueNud";
			this.EditValueNud.Size = new System.Drawing.Size(53, 20);
			this.EditValueNud.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 25);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "Value";
			// 
			// SelectGroupbox
			// 
			this.SelectGroupbox.Controls.Add(this.SelectedValueLabel);
			this.SelectGroupbox.Controls.Add(this.SelectIndexNud);
			this.SelectGroupbox.Controls.Add(this.label6);
			this.SelectGroupbox.Controls.Add(this.SelectButton);
			this.SelectGroupbox.Controls.Add(this.label5);
			this.SelectGroupbox.Location = new System.Drawing.Point(473, 95);
			this.SelectGroupbox.Name = "SelectGroupbox";
			this.SelectGroupbox.Size = new System.Drawing.Size(110, 105);
			this.SelectGroupbox.TabIndex = 34;
			this.SelectGroupbox.TabStop = false;
			this.SelectGroupbox.Text = "Select";
			// 
			// SelectedValueLabel
			// 
			this.SelectedValueLabel.AutoSize = true;
			this.SelectedValueLabel.Location = new System.Drawing.Point(46, 24);
			this.SelectedValueLabel.Name = "SelectedValueLabel";
			this.SelectedValueLabel.Size = new System.Drawing.Size(16, 13);
			this.SelectedValueLabel.TabIndex = 11;
			this.SelectedValueLabel.Text = "---";
			// 
			// SelectIndexNud
			// 
			this.SelectIndexNud.Location = new System.Drawing.Point(46, 48);
			this.SelectIndexNud.Name = "SelectIndexNud";
			this.SelectIndexNud.Size = new System.Drawing.Size(53, 20);
			this.SelectIndexNud.TabIndex = 10;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 24);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(34, 13);
			this.label6.TabIndex = 10;
			this.label6.Text = "Value";
			// 
			// SelectButton
			// 
			this.SelectButton.Location = new System.Drawing.Point(10, 72);
			this.SelectButton.Name = "SelectButton";
			this.SelectButton.Size = new System.Drawing.Size(89, 23);
			this.SelectButton.TabIndex = 11;
			this.SelectButton.Text = "Select";
			this.SelectButton.UseVisualStyleBackColor = true;
			this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 50);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(33, 13);
			this.label5.TabIndex = 3;
			this.label5.Text = "Index";
			// 
			// DeleteGroupbox
			// 
			this.DeleteGroupbox.Controls.Add(this.DeleteButton);
			this.DeleteGroupbox.Controls.Add(this.DeleteIndexNud);
			this.DeleteGroupbox.Controls.Add(this.label7);
			this.DeleteGroupbox.Location = new System.Drawing.Point(240, 94);
			this.DeleteGroupbox.Name = "DeleteGroupbox";
			this.DeleteGroupbox.Size = new System.Drawing.Size(110, 106);
			this.DeleteGroupbox.TabIndex = 32;
			this.DeleteGroupbox.TabStop = false;
			this.DeleteGroupbox.Text = "Delete";
			// 
			// DeleteButton
			// 
			this.DeleteButton.Location = new System.Drawing.Point(10, 74);
			this.DeleteButton.Name = "DeleteButton";
			this.DeleteButton.Size = new System.Drawing.Size(89, 23);
			this.DeleteButton.TabIndex = 8;
			this.DeleteButton.Text = "Delete";
			this.DeleteButton.UseVisualStyleBackColor = true;
			this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
			// 
			// DeleteIndexNud
			// 
			this.DeleteIndexNud.Location = new System.Drawing.Point(46, 48);
			this.DeleteIndexNud.Name = "DeleteIndexNud";
			this.DeleteIndexNud.Size = new System.Drawing.Size(53, 20);
			this.DeleteIndexNud.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(7, 51);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(33, 13);
			this.label7.TabIndex = 3;
			this.label7.Text = "Index";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(4, 9);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(51, 13);
			this.label8.TabIndex = 21;
			this.label8.Text = "Local list:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.LengthLabel);
			this.groupBox2.Controls.Add(this.CheckSizeButton);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Location = new System.Drawing.Point(356, 96);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(110, 104);
			this.groupBox2.TabIndex = 33;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Length";
			// 
			// LengthLabel
			// 
			this.LengthLabel.AutoSize = true;
			this.LengthLabel.Location = new System.Drawing.Point(50, 25);
			this.LengthLabel.Name = "LengthLabel";
			this.LengthLabel.Size = new System.Drawing.Size(16, 13);
			this.LengthLabel.TabIndex = 13;
			this.LengthLabel.Text = "---";
			// 
			// CheckSizeButton
			// 
			this.CheckSizeButton.Location = new System.Drawing.Point(13, 73);
			this.CheckSizeButton.Name = "CheckSizeButton";
			this.CheckSizeButton.Size = new System.Drawing.Size(89, 23);
			this.CheckSizeButton.TabIndex = 9;
			this.CheckSizeButton.Text = "Check";
			this.CheckSizeButton.UseVisualStyleBackColor = true;
			this.CheckSizeButton.Click += new System.EventHandler(this.CheckSizeButton_Click);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(10, 25);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(40, 13);
			this.label11.TabIndex = 12;
			this.label11.Text = "Length";
			// 
			// ConnectionSettingsGroupbox
			// 
			this.ConnectionSettingsGroupbox.Controls.Add(this.ListenPortTextBox);
			this.ConnectionSettingsGroupbox.Controls.Add(this.label12);
			this.ConnectionSettingsGroupbox.Controls.Add(this.ListenButton);
			this.ConnectionSettingsGroupbox.Controls.Add(this.HostPortTextbox);
			this.ConnectionSettingsGroupbox.Controls.Add(this.label10);
			this.ConnectionSettingsGroupbox.Controls.Add(this.HostAddressTextbox);
			this.ConnectionSettingsGroupbox.Controls.Add(this.label9);
			this.ConnectionSettingsGroupbox.Controls.Add(this.ConnectButton);
			this.ConnectionSettingsGroupbox.Location = new System.Drawing.Point(8, 210);
			this.ConnectionSettingsGroupbox.Name = "ConnectionSettingsGroupbox";
			this.ConnectionSettingsGroupbox.Size = new System.Drawing.Size(308, 104);
			this.ConnectionSettingsGroupbox.TabIndex = 35;
			this.ConnectionSettingsGroupbox.TabStop = false;
			this.ConnectionSettingsGroupbox.Text = "Connection settings";
			// 
			// ListenPortTextBox
			// 
			this.ListenPortTextBox.Location = new System.Drawing.Point(54, 49);
			this.ListenPortTextBox.Name = "ListenPortTextBox";
			this.ListenPortTextBox.Size = new System.Drawing.Size(74, 20);
			this.ListenPortTextBox.TabIndex = 12;
			this.ListenPortTextBox.Text = "8080";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(22, 52);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(26, 13);
			this.label12.TabIndex = 21;
			this.label12.Text = "Port";
			// 
			// ListenButton
			// 
			this.ListenButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ListenButton.Location = new System.Drawing.Point(11, 75);
			this.ListenButton.Name = "ListenButton";
			this.ListenButton.Size = new System.Drawing.Size(117, 23);
			this.ListenButton.TabIndex = 14;
			this.ListenButton.Text = "Listen";
			this.ListenButton.UseVisualStyleBackColor = true;
			this.ListenButton.Click += new System.EventHandler(this.ListenButton_Click);
			// 
			// HostPortTextbox
			// 
			this.HostPortTextbox.Location = new System.Drawing.Point(193, 23);
			this.HostPortTextbox.Name = "HostPortTextbox";
			this.HostPortTextbox.Size = new System.Drawing.Size(108, 20);
			this.HostPortTextbox.TabIndex = 15;
			this.HostPortTextbox.Text = "8080";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(167, 26);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(26, 13);
			this.label10.TabIndex = 16;
			this.label10.Text = "Port";
			// 
			// HostAddressTextbox
			// 
			this.HostAddressTextbox.Location = new System.Drawing.Point(193, 49);
			this.HostAddressTextbox.Name = "HostAddressTextbox";
			this.HostAddressTextbox.Size = new System.Drawing.Size(108, 20);
			this.HostAddressTextbox.TabIndex = 16;
			this.HostAddressTextbox.Text = "192.168.100.128";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(149, 52);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(44, 13);
			this.label9.TabIndex = 12;
			this.label9.Text = "Remote";
			// 
			// ConnectButton
			// 
			this.ConnectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ConnectButton.Location = new System.Drawing.Point(184, 75);
			this.ConnectButton.Name = "ConnectButton";
			this.ConnectButton.Size = new System.Drawing.Size(117, 23);
			this.ConnectButton.TabIndex = 17;
			this.ConnectButton.Text = "Connect";
			this.ConnectButton.UseVisualStyleBackColor = true;
			this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
			// 
			// LocalListContentLabel
			// 
			this.LocalListContentLabel.Location = new System.Drawing.Point(7, 33);
			this.LocalListContentLabel.Name = "LocalListContentLabel";
			this.LocalListContentLabel.Size = new System.Drawing.Size(576, 58);
			this.LocalListContentLabel.TabIndex = 20;
			this.LocalListContentLabel.Text = "Empty.";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(596, 324);
			this.Controls.Add(this.LocalListContentLabel);
			this.Controls.Add(this.ConnectionSettingsGroupbox);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.DeleteGroupbox);
			this.Controls.Add(this.SelectGroupbox);
			this.Controls.Add(this.EditGroupbox);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainForm";
			this.Text = "Distributed list";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.AddIndexNud)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.AddValueNud)).EndInit();
			this.EditGroupbox.ResumeLayout(false);
			this.EditGroupbox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.EditIndexNud)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.EditValueNud)).EndInit();
			this.SelectGroupbox.ResumeLayout(false);
			this.SelectGroupbox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.SelectIndexNud)).EndInit();
			this.DeleteGroupbox.ResumeLayout(false);
			this.DeleteGroupbox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.DeleteIndexNud)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ConnectionSettingsGroupbox.ResumeLayout(false);
			this.ConnectionSettingsGroupbox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button AddButton;
		private System.Windows.Forms.NumericUpDown AddIndexNud;
		private System.Windows.Forms.NumericUpDown AddValueNud;
		private System.Windows.Forms.GroupBox EditGroupbox;
		private System.Windows.Forms.Button EditButton;
		private System.Windows.Forms.NumericUpDown EditIndexNud;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown EditValueNud;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox SelectGroupbox;
		private System.Windows.Forms.Button SelectButton;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox DeleteGroupbox;
		private System.Windows.Forms.Button DeleteButton;
		private System.Windows.Forms.NumericUpDown DeleteIndexNud;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label SelectedValueLabel;
		private System.Windows.Forms.NumericUpDown SelectIndexNud;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label LengthLabel;
		private System.Windows.Forms.Button CheckSizeButton;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox ConnectionSettingsGroupbox;
		private System.Windows.Forms.Button ConnectButton;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox HostAddressTextbox;
		private System.Windows.Forms.Label LocalListContentLabel;
		private System.Windows.Forms.TextBox HostPortTextbox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button ListenButton;
		private System.Windows.Forms.TextBox ListenPortTextBox;
		private System.Windows.Forms.Label label12;
	}
}

