﻿using System;
using System.Net.Sockets;
using System.Windows.Forms;
using DistributedList.Properties;
using ListModel;

namespace DistributedList
{
	public partial class MainForm : Form
	{
		private readonly DistributedList<int> _distributedList;
		private readonly Timer listRefreshTimer = new Timer();

		public MainForm()
		{
			_distributedList = new DistributedList<int>();
			//_distributedList.Changed += PresentLocalList;
			InitializeComponent();
			listRefreshTimer.Interval = 500;
			listRefreshTimer.Tick += listRefreshTimer_Tick;
			listRefreshTimer.Start();
		}

		private void listRefreshTimer_Tick(object sender, EventArgs e)
		{
			PresentLocalList();
		}
		
		private void PresentLocalList()
		{
			LocalListContentLabel.Text = String.Join(" ", _distributedList.Local);
		}

		private void AddButton_Click(object sender, EventArgs e)
		{
			var index = (int)AddIndexNud.Value;
			var value = (int)AddValueNud.Value;
			_distributedList.Add(index, value);
		}

		private void EditButton_Click(object sender, EventArgs e)
		{
			var index = (int)EditIndexNud.Value;
			var value = (int)EditValueNud.Value;
			_distributedList.Edit(index, value);
		}
		
		private void DeleteButton_Click(object sender, EventArgs e)
		{
			var index = (int)DeleteIndexNud.Value;
			_distributedList.Delete(index);
		}

		private void SelectButton_Click(object sender, EventArgs e)
		{
			var index = (int)SelectIndexNud.Value;
			_distributedList.RequestSelect(index);
			SelectedValueLabel.Text = Resources.WaitForResponseTemporaryText;
		}

		private void CheckSizeButton_Click(object sender, EventArgs e)
		{
			_distributedList.RequestSize();
			LengthLabel.Text = Resources.WaitForResponseTemporaryText;
		}
		
		private void ConnectButton_Click(object sender, EventArgs e)
		{
			if (!_distributedList.IsConnected)
				Connect();
			else
				Disconnect();
		}
		
		private void ListenButton_Click(object sender, EventArgs e)
		{
			if (!_distributedList.IsListening)
				Listen();
			else
				CancelListening();
		}

		private void Connect()
		{
			var port = Int32.Parse(HostPortTextbox.Text);
			var address = HostAddressTextbox.Text;
			_distributedList.Connect(address, port, ProtocolType.Tcp);
			ConnectButton.Text = Resources.Disconnect;
		}

		private void Disconnect()
		{
			_distributedList.Disconnect();
			ConnectButton.Text = Resources.Connect;
		}

		private void Listen()
		{
			var port = Int32.Parse(ListenPortTextBox.Text);
			_distributedList.Listen(port, ProtocolType.Tcp);
			ListenButton.Text = Resources.CancelListening;
		}

		private void CancelListening()
		{
			_distributedList.CancelListening();
			ListenButton.Text = Resources.Listen;
		}
		
		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			_distributedList.Dispose();
		}

		~MainForm()
		{
			_distributedList.Dispose();
		}
	}
}
