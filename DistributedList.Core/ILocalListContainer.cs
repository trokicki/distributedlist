﻿using System.Collections.Generic;

namespace ListModel
{
	interface ILocalListContainer<T>
	{
		IList<T> Local { get; } 
	}
}
