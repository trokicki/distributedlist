﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListModel.Network
{
	public interface IProtocolQueryParser
	{
		ProtocolQuery Parse(string rawData);
		bool TryParse(string rawData, out ProtocolQuery query);
	}
}
