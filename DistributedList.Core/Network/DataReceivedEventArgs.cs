﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListModel.Network
{
	public class DataReceivedEventArgs : EventArgs
	{
		public string RawData { get; set; }
		public ProtocolQuery ParsedQuery { get; set; }

	}
}
