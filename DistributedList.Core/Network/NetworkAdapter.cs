﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ListModel.Exceptions;
using NLog;

namespace ListModel.Network
{
	public class NetworkAdapter<T> : INetworkAdapter<T>
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private NetworkFacilitatorSender<T> _networkFacilitatorSender;
		private NetworkFacilitatorReceiver<T> _networkFacilitatorReceiver;
		private readonly IProtocolQueryParser _protocolQueryParser = new Int32ProtocolQueryParser(ProtocolQuery.Delimeter);

		public event EventHandler<DataReceivedEventArgs> DataReceived;

		public bool IsListening { get; private set; }
		public bool IsConnected { get { return _networkFacilitatorSender != null && _networkFacilitatorSender.IsConnected; } }

		protected virtual void OnDataReceived(DataReceivedEventArgs eventArgs)
		{
			logger.Info("OnDataReceived triggered with query: {0}", eventArgs.ParsedQuery);
			var handler = DataReceived;
			if (handler != null)
				handler(this, eventArgs);
		}

		public string GetLocalIdentifier()
		{
			var address = Dns.GetHostAddresses(Dns.GetHostName()).First(a => a.AddressFamily == AddressFamily.InterNetwork);
			var port = _networkFacilitatorSender != null ? _networkFacilitatorReceiver.Port : new Random().Next(100000, 1000000);
			return String.Format("{0}:{1}", address, port);
		}
		
		//TODO: async
		public void Add(int index, T value) 
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.Add, GetLocalIdentifier())
				.AddParam(index).AddParam(value)
				.Build();
			SendAsync(query); 
		}

		//TODO: async
		public void Edit(int index, T value)
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.Edit, GetLocalIdentifier())
				.AddParam(index).AddParam(value)
				.Build();
			SendAsync(query);
		}

		//TODO: async
		public void Delete(int index)
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.Delete, GetLocalIdentifier())
				.AddParam(index)
				.Build();
			SendAsync(query);
		}

		//TODO: async
		public void RequestSize()
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.Size, GetLocalIdentifier()).Build();
			SendAsync(query);
		}

		//TODO: async
		public void ResponseSizeAsync(int size)
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.SizeResponse, GetLocalIdentifier())
				.AddParam(size)
				.Build();
			SendAsync(query);
		}

		//TODO: async
		public void RequestSelect(int index)
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.Select, GetLocalIdentifier())
				.AddParam(index)
				.Build();
			SendAsync(query);
		}

		//TODO: async
		public void ResponseSelect(int index, T value)
		{
			var query = new ProtocolQueryBuilder(ProtocolVerbs.SelectResponse, GetLocalIdentifier())
				.AddParam(index)
				.AddParam(value)
				.Build();
			SendAsync(query);
		}

		//TODO: async
		public void ForwardQuery(ProtocolQuery query)
		{
			SendAsync(query);
		}

		private async Task SendAsync(string payload)
		{
			if (_networkFacilitatorSender == null || !_networkFacilitatorSender.IsConnected)
				throw new NotConnectedException(String.Format("NetworkAdapter is not connected. Unsuccessful query: {0}", payload));
			await _networkFacilitatorSender.SendAsync(payload);
		}

		private async Task SendAsync(ProtocolQuery query)
		{
			await SendAsync(query.ToString());
		}

		public void Connect(string address, int port, ProtocolType protocolType)
		{
			logger.Info("Connecting to {0}:{1}", address, port);
			var socketType = protocolType == ProtocolType.Tcp ? SocketType.Stream : SocketType.Dgram;
			var socket = new Socket(AddressFamily.InterNetwork, socketType, protocolType);
			var ipHostInfo = Dns.GetHostEntry(address);
			var remoteIPv4 = ipHostInfo.AddressList.First(ad => ad.AddressFamily == AddressFamily.InterNetwork);
			_networkFacilitatorSender = new NetworkFacilitatorSender<T>
			{
				Socket = socket,
				RemoteAddress = remoteIPv4,
				Port = port
			};
			logger.Info("NetworkFacilitatorSender created with remote IPv4: {0}", remoteIPv4);
			_networkFacilitatorSender.Connect();
		}

		public void Disconnect()
		{
			logger.Info("Disconnecting NetworkFacilitatorSender");
			_networkFacilitatorSender.Disconnect();
		}

		public async Task Listen(int port, ProtocolType protocolType)
		{
			var localIPv4 = Dns.GetHostEntry(Dns.GetHostName())
				.AddressList.First(ad => ad.AddressFamily == AddressFamily.InterNetwork);

			var socketType = protocolType == ProtocolType.Tcp ? SocketType.Stream : SocketType.Dgram;
			var socket = new Socket(AddressFamily.InterNetwork, socketType, protocolType);
			_networkFacilitatorReceiver = new NetworkFacilitatorReceiver<T>
			{
				Socket = socket,
				Port = port,
				LocalAddress = localIPv4
			};
			logger.Info("NetworkFacilitatorReceiver created with local IPv4: {0}", localIPv4);
			logger.Info("Starting listening loop");
			IsListening = true;
			await Task.Run(() => ListeningLoop());
		}

		private void ListeningLoop()
		{
			_networkFacilitatorReceiver.Listen();
			_networkFacilitatorReceiver.HandlerSocket = _networkFacilitatorReceiver.Socket.Accept();
			logger.Info("Connection accepted, created HandlerSocket.");
			while (IsListening)
			{
				var bytes = new byte[1024];
				var bytesRec = _networkFacilitatorReceiver.HandlerSocket.Receive(bytes);
				var receivedPayload = Encoding.ASCII.GetString(bytes, 0, bytesRec);
				var query = _protocolQueryParser.Parse(receivedPayload);
				logger.Info("Received payload: {0} parsed to query: {1}", receivedPayload, query);
				OnDataReceived(new DataReceivedEventArgs { RawData = receivedPayload, ParsedQuery = query });
			}
		}

		public void CancelListening()
		{
			IsListening = false;
		}

		public void Dispose()
		{
			if (_networkFacilitatorSender != null) _networkFacilitatorSender.Dispose();
			if (_networkFacilitatorReceiver != null) _networkFacilitatorReceiver.Dispose();
		}

		~NetworkAdapter()
		{
			Dispose();
		}
	}
}
