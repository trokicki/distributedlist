﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace ListModel.Network
{
	public interface INetworkAdapter<T> : IDisposable, IDistributedList<T>
	{
		void Connect(string address, int port, ProtocolType protocolType);
		Task Listen(int port, ProtocolType protocolType);
		bool IsListening { get; }
		bool IsConnected { get; }
		void Disconnect();
		void CancelListening();
		event EventHandler<DataReceivedEventArgs> DataReceived;

		void Add(int index, T value);
		void Edit(int index, T value);
		void Delete(int index);

		void RequestSize();
		void ResponseSizeAsync(int size);

		void RequestSelect(int index);
		void ResponseSelect(int index, T value);

		string GetLocalIdentifier();

		void ForwardQuery(ProtocolQuery query);
	}
}
