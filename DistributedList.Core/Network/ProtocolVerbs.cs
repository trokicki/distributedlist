﻿namespace ListModel.Network
{
	public static class ProtocolVerbs
	{
		public const string Add = "ADD";
		public const string Edit = "EDIT";
		public const string Delete = "DELETE";
		public const string Size = "SIZE";
		public const string SizeResponse = "RESPONSE-SIZE";
		public const string Select = "SELECT";
		public const string SelectResponse = "RESPONSE-SELECT";
	}
}
