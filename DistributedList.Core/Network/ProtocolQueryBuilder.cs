namespace ListModel.Network
{
	class ProtocolQueryBuilder
	{
		private readonly ProtocolQuery _query;

		public ProtocolQueryBuilder(string verb, string identifier)
		{
			_query = new ProtocolQuery(verb, identifier);
		}
		
		public ProtocolQueryBuilder AddParam(object param)
		{
			_query.Params.Add(param);
			return this;
		}

		public ProtocolQuery Build()
		{
			return _query;
		}
	}
}