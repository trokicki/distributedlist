using System.Net;
using System.Net.Sockets;

namespace ListModel.Network
{
	class NetworkFacilitatorReceiver<T> : NetworkFacilitator<T>
	{
		public int Port { get; set; }
		public IPAddress LocalAddress { get; set; }
		public Socket HandlerSocket { get; set; }

		public void Listen()
		{
			if (EndPoint == null)
			{
				EndPoint = new IPEndPoint(LocalAddress, Port);
			}
			Socket.Bind(EndPoint);
			Socket.Listen(1);
			logger.Info("Started listening. Receiver main socket: {0}", Socket);
		}

		public override void Dispose()
		{
			base.Dispose();
			if (HandlerSocket == null) return;
			if(HandlerSocket.Connected)
				HandlerSocket.Shutdown(SocketShutdown.Both);
			HandlerSocket.Close();
			HandlerSocket.Dispose();
			HandlerSocket = null;
		}
	}
}