using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ListModel.Network
{
	class NetworkFacilitatorSender<T> : NetworkFacilitator<T>
	{
		public int Port { get; set; }
		public IPAddress RemoteAddress { get; set; }
		public bool IsConnected { get; private set; }
			
		public void Connect()
		{
			if (EndPoint == null)
			{
				EndPoint = new IPEndPoint(RemoteAddress, Port);
			}
			Socket.Connect(EndPoint);
			IsConnected = true;
			logger.Info("Connected socket: {0}", Socket);
		}
		
		public async Task SendAsync(string payload)
		{
			var messageBytes = Encoding.ASCII.GetBytes(payload);
			//TODO: temp
			await Task.Run(() => Socket.Send(messageBytes));
			logger.Info("Query sent to {0}:{1}. Payload: {2}", RemoteAddress, Port, payload);
		}

		public void Disconnect()
		{
			Socket.Disconnect(true);
			logger.Info("Socket disconnected");
		}
	}
}