﻿using System;
using System.Collections.Generic;
using ListModel.Exceptions;

namespace ListModel.Network
{
	public class Int32ProtocolQueryParser : IProtocolQueryParser
	{
		private readonly string _delimeter;

		public Int32ProtocolQueryParser(string delimeter)
		{
			_delimeter = delimeter;
		}

		public bool TryParse(string rawData, out ProtocolQuery query)
		{
			try
			{
				var segments = rawData.Split(Convert.ToChar(_delimeter));
				var verb = segments[0];
				var identifier = segments[1];

				//TODO: hardcoded list type
				var parameters = new List<int>();
				for (var i = 2; i < segments.Length; i++)
				{
					parameters.Add(Int32.Parse(segments[i]));
				}

				query = new ProtocolQuery(verb, identifier);
				foreach (var parameter in parameters)
					query.Params.Add(parameter);
				return true;
			}
			catch (Exception)
			{
				query = null;
			}
			return false;
		}

		public ProtocolQuery Parse(string rawData)
		{
			ProtocolQuery output;
			if (!TryParse(rawData, out output))
				throw new InvalidQueryStringException(String.Format("Cannot parse query. Received payload: {0}", rawData));
			return output;
		}
	}
}
