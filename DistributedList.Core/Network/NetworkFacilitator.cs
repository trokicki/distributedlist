using System;
using System.Net;
using System.Net.Sockets;
using NLog;

namespace ListModel.Network
{
	abstract class NetworkFacilitator<T> : IDisposable
	{
		public Socket Socket { get; set; }
		protected static Logger logger = LogManager.GetCurrentClassLogger();
		protected IPEndPoint EndPoint { get; set; }

		public virtual void Dispose()
		{
			if (Socket.Connected)
				Socket.Shutdown(SocketShutdown.Both);
			Socket.Close();
			Socket.Dispose();
			Socket = null;
		}
	}
}