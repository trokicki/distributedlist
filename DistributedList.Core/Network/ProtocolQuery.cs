using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ListModel.Exceptions;

namespace ListModel.Network
{
	public class ProtocolQuery
	{
		public const string Delimeter = "_";

		public string Verb { get; private set; }
		public string Identifier { get; private set; }
		public IList<object> Params { get; set; }

		public ProtocolQuery(string verb, string identifier)
		{
			Verb = verb;
			Identifier = identifier;
			Params = new List<object>();
		}

		public override string ToString()
		{
			
			var stringBuilder = new StringBuilder(Verb).Append(Delimeter).Append(Identifier);
			if (Params.Any())
			{
				var queryParams = String.Join(Delimeter.ToString(CultureInfo.InvariantCulture), Params);
				stringBuilder.Append(Delimeter).Append(queryParams);
			}									
			return stringBuilder.ToString();
		}
	}
}