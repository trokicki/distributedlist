﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListModel.Exceptions
{
	class InvalidQueryStringException : Exception
	{
		public InvalidQueryStringException() { }
		public InvalidQueryStringException(string message) : base(message) { }
		public InvalidQueryStringException(string message, Exception innerException) : base(message, innerException) { }
	}
}
