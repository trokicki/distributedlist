﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using ListModel.Exceptions;
using ListModel.Network;
using NLog;

namespace ListModel
{
	public class DistributedList<T> : IDisposable, IDistributedList<T>, ILocalListContainer<T>
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private readonly List<T> _localList;
		private readonly INetworkAdapter<T> _networkAdapter;

		public event EventHandler Changed;
		public IList<T> Local { get { return _localList.AsReadOnly(); } }

		//[ImportingConstructor]
		public DistributedList(/*INetworkAdapter<T> networkAdapter*/)
		{
			_networkAdapter = new NetworkAdapter<T>(); // networkAdapter;
			_networkAdapter.DataReceived += DataReceivedHandler;
			_localList = new List<T>();
		}

		protected virtual void OnListChanged()
		{
			logger.Info("OnListChanged raised.");
			var handler = Changed;
			if (handler != null)
				handler(this, EventArgs.Empty);
		}

		public void Add(int index, T value, bool sendQuery = true)
		{
			_localList.Insert(index, value);
			logger.Info("Add: {0} added on index {1}", value, index);
			if (sendQuery)
				_networkAdapter.Add(index, value);
			OnListChanged();
		}

		public void Edit(int index, T value, bool sendQuery = true)
		{
			_localList[index] = value;
			logger.Info("Edit: {0} set on index {1}", value, index);
			if (sendQuery)
				_networkAdapter.Edit(index, value);
			OnListChanged();
		}

		public void Delete(int index, bool sendQuery = true)
		{
			_localList.RemoveAt(index);
			logger.Info("Delete: value removed on index {0}", index);
			if (sendQuery)
				_networkAdapter.Delete(index);
			OnListChanged();
		}

		public void ResponseSize()
		{
			_networkAdapter.ResponseSizeAsync(_localList.Count);
		}

		public void RequestSize()
		{
			_networkAdapter.RequestSize();
		}

		public void ResponseSelect(int index)
		{
			_networkAdapter.ResponseSelect(index, _localList[index]);
		}

		public void RequestSelect(int index)
		{
			_networkAdapter.RequestSelect(index);
		}

		public void Dispose()
		{
			_networkAdapter.CancelListening();
			_networkAdapter.Dispose();
		}

		public void Connect(string address, int port, ProtocolType protocolType)
		{
			_networkAdapter.Connect(address, port, protocolType);
		}

		public void Disconnect()
		{
			_networkAdapter.Disconnect();
		}

		public void Listen(int port, ProtocolType protocolType)
		{
			_networkAdapter.Listen(port, protocolType);
		}

		public bool IsListening { get { return _networkAdapter.IsListening; } }
		public bool IsConnected { get { return _networkAdapter.IsConnected; } }

		public void CancelListening()
		{
			_networkAdapter.CancelListening();
		}

		private static void HandleSizeResponse(int size)
		{
			logger.Info("RESPONSE RECEIVED. Size of list is {0}", size);
			//TODO: raise event that will be exposed externally to allow e.g. GUI client to handle response
		}

		private static void HandleSelectResponse(int index, int value)
		{
			logger.Info("RESPONSE RECEIVED. Item at index {0} is {1}", index, value);
			//TODO: raise event that will be exposed externally to allow e.g. GUI client to handle response
		}

		private void DataReceivedHandler(object sender, DataReceivedEventArgs args)
		{
			var query = args.ParsedQuery;
			if (_networkAdapter.GetLocalIdentifier() == query.Identifier)
			{
				logger.Info("Query ran full ring, killing it. Query: {0}", query);
				return;
			}

			var forwardQuery = true;
			switch (args.ParsedQuery.Verb)
			{
				case ProtocolVerbs.Add:
					Add((int)query.Params[0], (T)query.Params[1], sendQuery: false);
					break;
				case ProtocolVerbs.Delete:
					Delete((int)query.Params[0], sendQuery: false);
					break;
				case ProtocolVerbs.Edit:
					Edit((int)query.Params[0], (T)query.Params[1], sendQuery: false);
					break;
				case ProtocolVerbs.Select:
					ResponseSelect((int)query.Params[0]);
					forwardQuery = false;
					break;
				case ProtocolVerbs.Size:
					ResponseSize();
					forwardQuery = false;
					break;
				case ProtocolVerbs.SelectResponse:
					HandleSelectResponse((int)query.Params[0], (int)query.Params[1]);
					break;
				case ProtocolVerbs.SizeResponse:
					HandleSizeResponse((int)query.Params[0]);
					break;
			}
			if (forwardQuery && _networkAdapter.IsConnected)
				_networkAdapter.ForwardQuery(query);
		}
	}
}
